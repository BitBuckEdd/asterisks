"""
WSGI config for asterisks project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""

import os
import sys 
#from django.core.wsgi import get_wsgi_application

#sys.path.append('/root/asterisks/asterisks/asterisks:/root/asterisks/asterisks/asterisks')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "asterisks.settings")
from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()

#call('python manage.py runserver 127.0.0.1:8000')
