from __future__ import absolute_import, unicode_literals

import os
import django
from celery import Celery
from celery.schedules import crontab
from django.conf import settings

# import set the default Django settings module for the 'celery' program.

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'asterisks.settings')
django.setup()
app = Celery('asterisks')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)


@app.task(bind=True)
def debug_task(self):

    print('Request: {0!r}'.format(self.request))


app.conf.beat_schedule = {

    'subsequent-calls': {  #name of the scheduler

        'task': 'project.tasks.subsequent_calls',  # task name which we have created in tasks.py

        'schedule': crontab(hour='8-18', minute=1, day_of_week='mon-sun')

    }

}
