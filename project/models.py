# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#import os
#import sys
from __future__ import unicode_literals


#from django.conf import settings



#from django.db.models import *
from django.db import models


class Cdr(models.Model):
    calldate = models.DateTimeField()
    clid =models.CharField(max_length=80)
    src = models.CharField(max_length=80)
    dst = models.CharField(max_length=80)
    dcontext = models.CharField(max_length=80)
    channel =models.CharField(max_length=80)
    dstchannel = models.CharField(max_length=80)
    lastapp = models.CharField(max_length=80)
    lastdata = models.CharField(max_length=80)
    duration = models.IntegerField()
    billsec = models.IntegerField()
    disposition = models.CharField(max_length=45)
    amaflags = models.IntegerField()
    accountcode = models.CharField(max_length=20)
    uniqueid = models.CharField(max_length=32, primary_key=True)
    userfield = models.CharField(max_length=255)
    did = models.CharField(max_length=50)
    recordingfile = models.CharField(max_length=255)
    cnum = models.CharField(max_length=80)
    callflag = models.IntegerField()
    cnam = models.CharField(max_length=80)
    outbound_cnum = models.CharField(max_length=80)
    outbound_cnam = models.CharField(max_length=80)
    dst_cnam = models.CharField(max_length=80)

    class Meta:
        managed = False
        db_table = 'cdr'
	ordering = ('-calldate',)


class CustomCdr(models.Model):
    calldate = models.DateTimeField()

    clid = models.CharField(max_length=80)

    src = models.CharField(max_length=80, null=True, blank=True)

    dst = models.CharField(max_length=80)

    dcontext = models.CharField(max_length=80)

    channel = models.CharField(max_length=80)

    dstchannel = models.CharField(max_length=80, null=True, blank=True)

    lastapp = models.CharField(max_length=80)

    lastdata = models.CharField(max_length=80)

    last_calldate = models.DateTimeField()

    next_calldate = models.DateTimeField()

    duration = models.IntegerField()

    billsec = models.IntegerField()

    disposition = models.CharField(max_length=45)

    accountcode = models.CharField(max_length=20, null=True, blank=True)

    uniqueid = models.CharField(max_length=32, primary_key=True)

    userfield = models.CharField(max_length=255)

    did = models.CharField(max_length=50)

    recordingfile = models.CharField(max_length=255)

    cnum = models.CharField(max_length=80)

    callflag = models.IntegerField()

    cnam = models.CharField(max_length=80)

    outbound_cnum = models.CharField(max_length=80)
    
