import sys

from datetime import datetime

from datetime import timedelta

from pycall import CallFile, Call, Context

def call(number, time=None, command = 'from-internal-intro',par='_X.',values='1'):

        c = Call('DAHDI/g0/%s' % number)

        con = Context(command, par, values)

        cf = CallFile(c, con, user='asterisk')

        cf.spool(time)

if __name__ == '__main__':

        call(sys.argv[1], datetime.now()+timedelta(seconds=1))
